//
//  ViewController.m
//  BreakoutMasterDetailDemo
//
//  Created by James Cash on 16-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "DemoTableViewCell.h"
#import "DetailViewController.h"

@interface ViewController () <UITableViewDataSource,DetailUpdateDelegate>

@property (nonatomic,strong) NSMutableArray *data;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.data = [@[@"Foo", @"Bar", @"Baz"] mutableCopy];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DemoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];

    NSString *theThing = self.data[indexPath.row];
    cell.whateverLabel.text = theThing;

    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"detailSegue"]) {
//        NSString *data = ((DemoTableViewCell*)sender).whateverLabel.text;
        NSIndexPath *path = [self.tableView indexPathForSelectedRow];
        NSString *data = self.data[path.row];
        DetailViewController *dvc = segue.destinationViewController;

        //dvc.thingLabel.text = data; //doesn't work!
        dvc.detailInfo = data;

        dvc.delegate = self;
    }
}

- (void)detail:(DetailViewController *)detail updated:(NSString *)oldString toNewValue:(NSString *)newString
{
    NSUInteger idx = [self.data indexOfObject:oldString];
    self.data[idx] = newString;
    [self.tableView reloadData];
}

@end
