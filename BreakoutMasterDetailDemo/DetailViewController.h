//
//  DetailViewController.h
//  BreakoutMasterDetailDemo
//
//  Created by James Cash on 16-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@protocol DetailUpdateDelegate

- (void)detail:(DetailViewController*)detail
       updated:(NSString*)oldString
    toNewValue:(NSString*)newString;

@end

@interface DetailViewController : UIViewController
@property (strong, nonatomic) NSString *detailInfo;

@property (weak, nonatomic) id<DetailUpdateDelegate> delegate;

@end
