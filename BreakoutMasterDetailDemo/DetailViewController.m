//
//  DetailViewController.m
//  BreakoutMasterDetailDemo
//
//  Created by James Cash on 16-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()
@property (strong, nonatomic) IBOutlet UILabel *thingLabel;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.thingLabel.text = self.detailInfo;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)makeExciting:(id)sender {
    NSString *oldValue = self.detailInfo;
    self.detailInfo = [self.detailInfo stringByAppendingString:@"!"];
    self.thingLabel.text = self.detailInfo;
    [self.delegate detail:self updated:oldValue toNewValue:self.detailInfo];
}

@end
