//
//  DemoTableViewCell.m
//  BreakoutMasterDetailDemo
//
//  Created by James Cash on 16-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "DemoTableViewCell.h"



@implementation DemoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
