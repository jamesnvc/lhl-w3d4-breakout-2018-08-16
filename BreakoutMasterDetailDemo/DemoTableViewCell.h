//
//  DemoTableViewCell.h
//  BreakoutMasterDetailDemo
//
//  Created by James Cash on 16-08-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DemoTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *whateverLabel;

@end
